## R code Collection

- AIG Client Segmentation Project Setup code
- AIG Client Segmentation Project 1/20 runs model code
- AIG Client Segmentation Project Insight Extraction code
- Interactive SVG Table Code



### Misc

- [Author for most popular R pkgs](https://en.wikipedia.org/wiki/Hadley_Wickham)

- [Three methods for music recommendations](https://hackernoon.com/spotifys-discover-weekly-how-machine-learning-finds-your-new-music-19a41ab76efe)


- [Pairwise-complete correlation considered dangerous](http://bwlewis.github.io/covar/missing.html)


- [Missing Value Imputation](http://www.stat.columbia.edu/~gelman/arm/missing.pdf

- [Precision and Recall](https://stats.stackexchange.com/questions/62621/recall-and-precision-in-classification)

- [Handling Class Imbalance in Direct Marketing](https://research.gold.ac.uk/17248/1/2016_SAI_Computing_IEEE_Class_imbalance.pdf)

- [Choosing Logisitc Regression's cutoff value for Unbalanced dataset](http://ethen8181.github.io/machine-learning/unbalanced/unbalanced.html)

### TODO
- Density plots/histogram plots in ggplot2
- multiplot functions
- 
